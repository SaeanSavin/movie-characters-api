﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTO.Character;
using MovieCharactersAPI.Models.DTO.Movie;
using MovieCharactersAPI.Services.Characters;
using MovieCharactersAPI.Services.Movies;
using MovieCharactersAPI.Util.Exceptions;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/v1/characters")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICharacterService _characterService;

        public CharactersController(IMapper mapper, ICharacterService characterService)
        {
            _mapper = mapper;
            _characterService = characterService;
        }

        /// <summary>
        /// Get all the characters in the database with their related data represented as IDs
        /// </summary>
        /// <returns>List of MovieDTO</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterDTO>>> GetCharactersAsync()
        {
            return Ok(
                _mapper.Map<List<CharacterDTO>>(
                    await _characterService.GetAllAsync()
                )
            );
        }

        /// <summary>
        /// Returns a character by its character id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterDTO>> GetCharacterByIdAsync(int id)
        {
            try
            {
                return Ok(_mapper.Map<CharacterDTO>(
                        await _characterService.GetByIdAsync(id))
                    );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = (int)HttpStatusCode.NotFound
                    }
                );
            }
        }

        /// <summary>
        /// Updates a character of a given Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="character"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacterAsync(int id, CharacterPutDTO character)
        {
            if (id != character.CharacterId) return BadRequest();

            try
            {
                await _characterService.UpdateAsync(
                     _mapper.Map<Character>(character)
                 );
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = (int)HttpStatusCode.NotFound
                    }
                );
            }
        }

        /// <summary>
        /// Creates a character in the database
        /// </summary>
        /// <param name="characterDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostCharacterAsync(CharacterPostDTO characterDto)
        {
            Character character = _mapper.Map<Character>(characterDto);
            await _characterService.AddAsync(character);
            return CreatedAtAction("GetCharacterById", new { id = character.CharacterId }, character);
        }


        /// <summary>
        /// Deletes a character of a given Id in the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacterAsync(int id)
        {
            try
            {
                await _characterService.DeleteByIdAsync(id);

                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = (int)HttpStatusCode.NotFound
                    }
                );
            }
        }

        /// <summary>
        /// Returns the movies of a certain charcter of a given customer Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieSummaryDTO>>> GetMoviesForCharacterAsync(int id)
        {
            try
            {
                return Ok(
                        _mapper.Map<List<MovieSummaryDTO>>(
                            await _characterService.GetMoviesForCharacterAsync(id)
                        )
                    );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = (int)HttpStatusCode.NotFound
                    }
                );
            }
        }

        /// <summary>
        /// Updates the movies that a given character by a character Id is featured in
        /// /// </summary>
        /// <param name="movieIds"></param>
        /// <param name="id"></param>
        /// <returns></returns>

        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateMoviesForCharacterAsync(int[] movieIds, int id)
        {
            try
            {
                await _characterService.UpdateMoviesForCharacterAsync(movieIds, id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = (int)HttpStatusCode.NotFound
                    }
                );
            }
        }
        

    }
}
