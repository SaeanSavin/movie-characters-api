﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTO.Character;
using MovieCharactersAPI.Models.DTO.Franchise;
using MovieCharactersAPI.Models.DTO.Movie;
using MovieCharactersAPI.Services.Franchises;
using MovieCharactersAPI.Util.Exceptions;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/v1/franchise")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IFranchiseService _franchiseService;

        public FranchisesController(IMapper mapper, IFranchiseService movieService)
        {
            _mapper = mapper;
            _franchiseService = movieService;
        }

        /// <summary>
        /// Get all the franchises in the database with their related data represented as IDs
        /// </summary>
        /// <returns>List of MovieDTO</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseDTO>>> GetFranchisesAsync()
        {
            return Ok(
                _mapper.Map<List<FranchiseDTO>>(
                    await _franchiseService.GetAllAsync()
                )
            );
        }

        /// <summary>
        /// Creates a franchises in the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDTO>> GetFranchiseByIdAsync(int id)
        {
            try
            {
                return Ok(_mapper.Map<FranchiseDTO>(
                        await _franchiseService.GetByIdAsync(id))
                    );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = (int)HttpStatusCode.NotFound
                    }
                );
            }
        }

        /// <summary>
        /// Updates a frachise of a given franchise Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchise"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchiseAsync(int id, FranchisePutDTO franchise)
        {
            if (id != franchise.FranchiseId) return BadRequest();

            try
            {
                await _franchiseService.UpdateAsync(
                     _mapper.Map<Franchise>(franchise)
                 );
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = (int)HttpStatusCode.NotFound
                    }
                );
            }
        }

        /// <summary>
        /// Creates a franchise in the database
        /// </summary>
        /// <param name="franchiseDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostFranchiseAsync(FranchisePostDTO franchiseDto)
        {
            Franchise franchise = _mapper.Map<Franchise>(franchiseDto);
            await _franchiseService.AddAsync(franchise);
            return CreatedAtAction("GetFranchiseById", new { id = franchise.FranchiseId }, franchise);
        }

        /// <summary>
        /// Deletes a franchise of a given Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchiseAsync(int id)
        {
            try
            {
                await _franchiseService.DeleteByIdAsync(id);

                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = (int)HttpStatusCode.NotFound
                    }
                );
            }
        }

        /// <summary>
        /// Returns all movies from a franchise of a given Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieSummaryDTO>>> GetMoviesForFranchiseAsync(int id)
        {
            try
            {
                return Ok(
                        _mapper.Map<List<MovieSummaryDTO>>(
                            await _franchiseService.GetMoviesForFranchiseAsync(id)
                        )
                    );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = (int)HttpStatusCode.NotFound
                    }
                );
            }
        }

        /// <summary>
        /// Returns all characters from a franchise with a given franchise Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterSummaryDTO>>> GetCharactersForFranchiseAsync(int id)
        {
            try
            {
                return Ok(
                        _mapper.Map<List<CharacterSummaryDTO>>(
                            await _franchiseService.GetCharactersForFranchiseAsync(id)
                        )
                    );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = (int)HttpStatusCode.NotFound
                    }
                );
            }
        }

        /// <summary>
        /// Update movies to a franchise of a given Id
        /// </summary>
        /// <param name="movieIds"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateMoviesForFranchise(int[] movieIds, int id)
        {
            try
            {
                await _franchiseService.UpdateMoviesInFranchiseAsync(movieIds, id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = (int)HttpStatusCode.NotFound
                    }
                );
            }
        }
    }
}
