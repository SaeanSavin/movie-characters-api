﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTO.Character;
using MovieCharactersAPI.Models.DTO.Franchise;
using MovieCharactersAPI.Models.DTO.Movie;
using MovieCharactersAPI.Services.Movies;
using MovieCharactersAPI.Util.Exceptions;
using System.Net;

namespace MovieCharactersAPI.Controllers
{

    [Route("api/v1/movies")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;

        public MoviesController(IMapper mapper, IMovieService movieService)
        {
            _mapper = mapper;
            _movieService = movieService;
        }

        /// <summary>
        /// Get all the movies in the database with their related data represented as IDs
        /// </summary>
        /// <returns>List of MovieDTO</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieDTO>>> GetMoviesAsync()
        {
            return Ok(
                _mapper.Map<List<MovieDTO>>(
                    await _movieService.GetAllAsync()
                )
            );
        }

        /// <summary>
        /// Returns a movie based in its movie Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDTO>> GetMovieByIdAsync(int id)
        {
            try
            {
                return Ok(_mapper.Map<MovieDTO>(
                        await _movieService.GetByIdAsync(id))
                    );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = (int)HttpStatusCode.NotFound
                    }
                );
            }
        }

        /// <summary>
        /// Updates a movie, identified by its movie Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieDto"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovieAsync(int id, MoviePutDTO movieDto)
        {
            if (id != movieDto.MovieId) return BadRequest();

            try
            {
               await _movieService.UpdateAsync(
                    _mapper.Map<Movie>(movieDto)
                );
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            { 
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = (int)HttpStatusCode.NotFound
                    }
                );
            }
        }

        /// <summary>
        /// Creates a movie in the database
        /// </summary>
        /// <param name="movieDto"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostMovieAsync(MoviePostDTO movieDto)
        {
            Movie movie = _mapper.Map<Movie>(movieDto);
            await _movieService.AddAsync(movie);
            return CreatedAtAction("GetMovieById", new { id = movie.MovieId }, movie);
        }

        /// <summary>
        /// Deletes a movie of a given movie Id in the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovieAsync(int id)
        {
            try
            {
                await _movieService.DeleteByIdAsync(id);
                
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = (int)HttpStatusCode.NotFound
                    }
                );
            }
        }

        /// <summary>
        /// Returns the characters featured in a movie of a given movie Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterSummaryDTO>>> GetCharactersForMovieAsync(int id)
        {
            try
            {
                return Ok(
                        _mapper.Map<List<CharacterSummaryDTO>>(
                            await _movieService.GetCharactersInMovieAsync(id)    
                        )
                    );
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = (int)HttpStatusCode.NotFound
                    }
                );
            }
        }

        /// <summary>
        /// Updates the characters of a movie of given movie Id
        /// </summary>
        /// <param name="characterIds"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("{id}/charaters")]
        public async Task<IActionResult> UpdateCharactersForMovie(int[] characterIds, int id)
        {
            try
            {
               await _movieService.UpdateCharactersAsync(characterIds, id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = (int)HttpStatusCode.NotFound
                    }
                );
            }
        }

        /// <summary>
        /// Updates a the franchise of movie given by a movie Id
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("{id}/franchise")]
        public async Task<IActionResult> UpdateFranchiseForMovie(int franchiseId, int id)
        {
            try
            {
                await _movieService.UpdateFranchiseAsync(franchiseId, id);
                return NoContent();
            }
            catch (EntityNotFoundException ex)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = ex.Message,
                        Status = (int)HttpStatusCode.NotFound
                    }
                );
            }
        }
    }
}
