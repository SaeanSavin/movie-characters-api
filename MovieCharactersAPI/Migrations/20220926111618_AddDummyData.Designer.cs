﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using MovieCharactersAPI.Models;

#nullable disable

namespace MovieCharactersAPI.Migrations
{
    [DbContext(typeof(MovieDbContext))]
    [Migration("20220926111618_AddDummyData")]
    partial class AddDummyData
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.9")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("CharacterMovie", b =>
                {
                    b.Property<int>("CharactersCharacterId")
                        .HasColumnType("int");

                    b.Property<int>("MoviesMovieId")
                        .HasColumnType("int");

                    b.HasKey("CharactersCharacterId", "MoviesMovieId");

                    b.HasIndex("MoviesMovieId");

                    b.ToTable("CharacterMovie");
                });

            modelBuilder.Entity("MovieCharactersAPI.Models.Domain.Character", b =>
                {
                    b.Property<int>("CharacterId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("CharacterId"), 1L, 1);

                    b.Property<string>("Alias")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Fullname")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Gender")
                        .HasMaxLength(20)
                        .HasColumnType("nvarchar(20)");

                    b.Property<string>("Picture")
                        .HasMaxLength(200)
                        .HasColumnType("nvarchar(200)");

                    b.HasKey("CharacterId");

                    b.ToTable("Characters");

                    b.HasData(
                        new
                        {
                            CharacterId = 1,
                            Fullname = "Keanu Reeves",
                            Gender = "Male"
                        },
                        new
                        {
                            CharacterId = 2,
                            Fullname = "Carrie-Anne Moss",
                            Gender = "Female"
                        },
                        new
                        {
                            CharacterId = 3,
                            Alias = "Michael J. Fox",
                            Fullname = "Michael Andrew Fox",
                            Gender = "Male"
                        },
                        new
                        {
                            CharacterId = 4,
                            Fullname = "Christopher Lloyd",
                            Gender = "Male"
                        });
                });

            modelBuilder.Entity("MovieCharactersAPI.Models.Domain.Franchise", b =>
                {
                    b.Property<int>("FranchiseId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("FranchiseId"), 1L, 1);

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasMaxLength(200)
                        .HasColumnType("nvarchar(200)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.HasKey("FranchiseId");

                    b.ToTable("Franchise");

                    b.HasData(
                        new
                        {
                            FranchiseId = 1,
                            Description = "Back to the Future is an American science fiction comedy franchise",
                            Name = "Back to the Future"
                        },
                        new
                        {
                            FranchiseId = 2,
                            Description = "The Matrix is an American media franchise consisting of four feature films.",
                            Name = "The Matrix"
                        });
                });

            modelBuilder.Entity("MovieCharactersAPI.Models.Domain.Movie", b =>
                {
                    b.Property<int>("MovieId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("MovieId"), 1L, 1);

                    b.Property<string>("Director")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<int?>("FranchiseId")
                        .HasColumnType("int");

                    b.Property<string>("Genre")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Picture")
                        .IsRequired()
                        .HasMaxLength(200)
                        .HasColumnType("nvarchar(200)");

                    b.Property<DateTime>("ReleaseYear")
                        .HasColumnType("datetime2");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.Property<string>("Trailer")
                        .IsRequired()
                        .HasMaxLength(200)
                        .HasColumnType("nvarchar(200)");

                    b.HasKey("MovieId");

                    b.HasIndex("FranchiseId");

                    b.ToTable("Movie", (string)null);

                    b.HasData(
                        new
                        {
                            MovieId = 1,
                            Director = "Robert Zemeckis",
                            FranchiseId = 1,
                            Genre = "Science Fiction",
                            Picture = "",
                            ReleaseYear = new DateTime(1985, 7, 3, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Title = "Back to the Future",
                            Trailer = ""
                        },
                        new
                        {
                            MovieId = 2,
                            Director = "Robert Zemeckis",
                            FranchiseId = 1,
                            Genre = "Science Fiction",
                            Picture = "",
                            ReleaseYear = new DateTime(1989, 11, 22, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Title = "Back to the Future II",
                            Trailer = ""
                        },
                        new
                        {
                            MovieId = 3,
                            Director = "Robert Zemeckis",
                            FranchiseId = 1,
                            Genre = "Science Fiction",
                            Picture = "",
                            ReleaseYear = new DateTime(1990, 5, 25, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Title = "Back to the Future III",
                            Trailer = ""
                        },
                        new
                        {
                            MovieId = 4,
                            Director = "The Wachowskis",
                            FranchiseId = 2,
                            Genre = "Science Fiction",
                            Picture = "",
                            ReleaseYear = new DateTime(1999, 3, 31, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Title = "The Matrix",
                            Trailer = ""
                        },
                        new
                        {
                            MovieId = 5,
                            Director = "The Wachowskis",
                            FranchiseId = 2,
                            Genre = "Science Fiction",
                            Picture = "",
                            ReleaseYear = new DateTime(2003, 5, 15, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Title = "The Matrix Reloaded",
                            Trailer = ""
                        },
                        new
                        {
                            MovieId = 6,
                            Director = "The Wachowskis",
                            FranchiseId = 2,
                            Genre = "Science Fiction",
                            Picture = "",
                            ReleaseYear = new DateTime(2003, 11, 3, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Title = "The Matrix Revolutions",
                            Trailer = ""
                        },
                        new
                        {
                            MovieId = 7,
                            Director = "Lana Wachowski",
                            FranchiseId = 2,
                            Genre = "Science Fiction",
                            Picture = "",
                            ReleaseYear = new DateTime(2021, 12, 22, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            Title = "The Matrix Resurrections",
                            Trailer = ""
                        });
                });

            modelBuilder.Entity("CharacterMovie", b =>
                {
                    b.HasOne("MovieCharactersAPI.Models.Domain.Character", null)
                        .WithMany()
                        .HasForeignKey("CharactersCharacterId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("MovieCharactersAPI.Models.Domain.Movie", null)
                        .WithMany()
                        .HasForeignKey("MoviesMovieId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("MovieCharactersAPI.Models.Domain.Movie", b =>
                {
                    b.HasOne("MovieCharactersAPI.Models.Domain.Franchise", "Franchise")
                        .WithMany("Movies")
                        .HasForeignKey("FranchiseId");

                    b.Navigation("Franchise");
                });

            modelBuilder.Entity("MovieCharactersAPI.Models.Domain.Franchise", b =>
                {
                    b.Navigation("Movies");
                });
#pragma warning restore 612, 618
        }
    }
}
