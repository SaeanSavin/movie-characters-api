﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MovieCharactersAPI.Migrations
{
    public partial class AddDummyData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "CharacterId", "Alias", "Fullname", "Gender", "Picture" },
                values: new object[,]
                {
                    { 1, null, "Keanu Reeves", "Male", null },
                    { 2, null, "Carrie-Anne Moss", "Female", null },
                    { 3, "Michael J. Fox", "Michael Andrew Fox", "Male", null },
                    { 4, null, "Christopher Lloyd", "Male", null }
                });

            migrationBuilder.InsertData(
                table: "Franchise",
                columns: new[] { "FranchiseId", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Back to the Future is an American science fiction comedy franchise", "Back to the Future" },
                    { 2, "The Matrix is an American media franchise consisting of four feature films.", "The Matrix" }
                });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "MovieId", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[,]
                {
                    { 1, "Robert Zemeckis", 1, "Science Fiction", "", new DateTime(1985, 7, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "Back to the Future", "" },
                    { 2, "Robert Zemeckis", 1, "Science Fiction", "", new DateTime(1989, 11, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "Back to the Future II", "" },
                    { 3, "Robert Zemeckis", 1, "Science Fiction", "", new DateTime(1990, 5, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Back to the Future III", "" },
                    { 4, "The Wachowskis", 2, "Science Fiction", "", new DateTime(1999, 3, 31, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Matrix", "" },
                    { 5, "The Wachowskis", 2, "Science Fiction", "", new DateTime(2003, 5, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Matrix Reloaded", "" },
                    { 6, "The Wachowskis", 2, "Science Fiction", "", new DateTime(2003, 11, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Matrix Revolutions", "" },
                    { 7, "Lana Wachowski", 2, "Science Fiction", "", new DateTime(2021, 12, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Matrix Resurrections", "" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "CharacterId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "CharacterId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "CharacterId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "CharacterId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Franchise",
                keyColumn: "FranchiseId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Franchise",
                keyColumn: "FranchiseId",
                keyValue: 2);
        }
    }
}
