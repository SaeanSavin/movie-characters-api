﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MovieCharactersAPI.Migrations
{
    public partial class removedreleaseyear : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReleaseYear",
                table: "Movie");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "ReleaseYear",
                table: "Movie",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 1,
                column: "ReleaseYear",
                value: new DateTime(1985, 7, 3, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 2,
                column: "ReleaseYear",
                value: new DateTime(1989, 11, 22, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 3,
                column: "ReleaseYear",
                value: new DateTime(1990, 5, 25, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 4,
                column: "ReleaseYear",
                value: new DateTime(1999, 3, 31, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 5,
                column: "ReleaseYear",
                value: new DateTime(2003, 5, 15, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 6,
                column: "ReleaseYear",
                value: new DateTime(2003, 11, 3, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 7,
                column: "ReleaseYear",
                value: new DateTime(2021, 12, 22, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
