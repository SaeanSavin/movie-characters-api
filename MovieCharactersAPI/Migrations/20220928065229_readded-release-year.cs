﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MovieCharactersAPI.Migrations
{
    public partial class readdedreleaseyear : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ReleaseYear",
                table: "Movie",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 1,
                column: "ReleaseYear",
                value: 1985);

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 2,
                column: "ReleaseYear",
                value: 1989);

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 3,
                column: "ReleaseYear",
                value: 1990);

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 4,
                column: "ReleaseYear",
                value: 1999);

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 5,
                column: "ReleaseYear",
                value: 2003);

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 6,
                column: "ReleaseYear",
                value: 2003);

            migrationBuilder.UpdateData(
                table: "Movie",
                keyColumn: "MovieId",
                keyValue: 7,
                column: "ReleaseYear",
                value: 2021);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ReleaseYear",
                table: "Movie");
        }
    }
}
