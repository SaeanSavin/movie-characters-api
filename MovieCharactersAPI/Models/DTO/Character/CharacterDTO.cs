﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models.DTO.Character
{
    public class CharacterDTO
    {
        public int CharacterId { get; set; }
        [MaxLength(50)] public string Fullname { get; set; } = null!;

        [MaxLength(50)] public string? Alias { get; set; }

        [MaxLength(20)] public string? Gender { get; set; }

        [MaxLength(200)] public string? Picture { get; set; }

        public List<int>? Movies { get; set; }
    }
}

