﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models.DTO.Character
{
    public class CharacterPutDTO
    {
        public int CharacterId { get; set; }

        [MaxLength(100)] public string Fullname { get; set; } = null!;

        [MaxLength(100)] public string? Alias { get; set; }

        [MaxLength(100)] public string? Gender { get; set; }

        [MaxLength(100)] public string? Picture { get; set; }
    }
}

