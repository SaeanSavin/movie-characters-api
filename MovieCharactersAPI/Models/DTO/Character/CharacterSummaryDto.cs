﻿namespace MovieCharactersAPI.Models.DTO.Character
{
    public class CharacterSummaryDTO
    {
        public int CharacterId { get; set; }
        public string FullName { get; set; } = null!;
    }
}
