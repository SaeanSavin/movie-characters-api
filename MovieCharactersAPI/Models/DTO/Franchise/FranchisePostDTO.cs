﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models.DTO.Franchise
{
    public class FranchisePostDTO
    {
        [MaxLength(200)] public string Name { get; set; } = null!;

        [MaxLength(200)] public string? Description { get; set; }
    }
}
