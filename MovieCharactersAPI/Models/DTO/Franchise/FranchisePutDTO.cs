﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models.DTO.Franchise
{
    public class FranchisePutDTO
    {
        public int FranchiseId { get; set; }

        public string Name { get; set; } = null!;

        public string? Description { get; set; }

    }
}
