﻿namespace MovieCharactersAPI.Models.DTO.Franchise
{
    public class FranchiseSummaryDTO
    {
        public int FranchiseId { get; set; }

        public string Name { get; set; } = null!;
    }
}
