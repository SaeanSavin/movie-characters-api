﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models.DTO.Movie
{
    public class MoviePostDTO
    {
        public string Title { get; set; } = null!;

        public string Genre { get; set; } = null!;

        public int ReleaseYear { get; set; }

        public string Director { get; set; } = null!;

        public string? Picture { get; set; }

        public string? Trailer { get; set; }
    }
}
