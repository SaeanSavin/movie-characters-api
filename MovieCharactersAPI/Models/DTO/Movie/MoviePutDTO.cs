﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharactersAPI.Models.DTO.Movie
{
    public class MoviePutDTO
    {
        public int MovieId { get; set; }

        [MaxLength(100)] public string Title { get; set; } = null!;

        [MaxLength(50)] public string Genre { get; set; } = null!;

        public int ReleaseYear { get; set; }

        [MaxLength(50)] public string Director { get; set; } = null!;

        [MaxLength(200)] public string? Picture { get; set; }

        [MaxLength(200)] public string? Trailer { get; set; }
    }
}
