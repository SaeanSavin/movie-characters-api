﻿namespace MovieCharactersAPI.Models.DTO.Movie
{
    public class MovieSummaryDTO
    {
        public int MovieId { get; set; }
        public string Title { get; set; } = null!;
    }
}
