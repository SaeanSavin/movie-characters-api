﻿using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.Models.Domain
{
    public class Character
    {
        public Character()
        {
            Movies = new List<Movie>();
        }

        public int CharacterId { get; set; }
        [MaxLength(50)] public string Fullname { get; set; } = null!;
        [MaxLength(50)] public string? Alias { get; set; }
        [MaxLength(20)] public string? Gender { get; set; }
        [MaxLength(200)] public string? Picture { get; set; }

        public ICollection<Movie> Movies { get; set; } = null!;
    }
}
