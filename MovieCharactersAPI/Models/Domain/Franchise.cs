﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieCharactersAPI.Models.Domain
{
    [Table("Franchise")]
    public class Franchise
    {
        public Franchise()
        {
            Movies = new HashSet<Movie>();
        }

        public int FranchiseId { get; set; }
        [MaxLength(50)] public string Name { get; set; } = null!;
        [MaxLength(200)] public string Description { get; set; } = null!;

        public ICollection<Movie> Movies { get; set; }
    }
}
