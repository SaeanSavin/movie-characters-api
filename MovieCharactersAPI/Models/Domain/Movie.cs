﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieCharactersAPI.Models.Domain
{
    [Table("Movie")]
    public class Movie
    {
        public Movie()
        {
            Characters = new HashSet<Character>();
        }

        public int MovieId { get; set; }
        [MaxLength(100)] public string Title { get; set; } = null!;
        [MaxLength(50)] public string Genre { get; set; } = null!;
        public int ReleaseYear { get; set; }
        [MaxLength(50)] public string Director { get; set; } = null!;
        [MaxLength(200)] public string? Picture { get; set; }
        [MaxLength(200)] public string? Trailer { get; set; }

        public int? FranchiseId { get; set; }
        public Franchise? Franchise { get; set; }

        public ICollection<Character> Characters { get; set; }
    }
}
