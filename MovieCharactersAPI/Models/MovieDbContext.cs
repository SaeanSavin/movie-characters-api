﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models.Domain;

namespace MovieCharactersAPI.Models
{
    public class MovieDbContext : DbContext
    {
        public MovieDbContext() {}

        public MovieDbContext(DbContextOptions<MovieDbContext> options) : base(options) {}

        public DbSet<Movie> Movies { get; set; } = null!;
        public DbSet<Character> Characters { get; set; } = null!;   
        public DbSet<Franchise> Franchises { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            Franchise backToTheFuture = new()
            {
                FranchiseId = 1,
                Name = "Back to the Future",
                Description = "Back to the Future is an American science fiction comedy franchise"
            };
            Franchise matrix = new()
            {
                FranchiseId = 2,
                Name = "The Matrix",
                Description = "The Matrix is an American media franchise consisting of four feature films."
            };

            Movie backToTheFutureI = new()
            {
                MovieId = 1,
                Title = "Back to the Future",
                Genre = "Science Fiction",
                ReleaseYear = 1985,
                Director = "Robert Zemeckis",
                Picture = "",
                Trailer = "",
                FranchiseId = 1
            };
            Movie backToTheFutureII = new()
            {
                MovieId = 2,
                Title = "Back to the Future II",
                Genre = "Science Fiction",
                ReleaseYear = 1989,
                Director = "Robert Zemeckis",
                Picture = "",
                Trailer = "",
                FranchiseId = 1
            };
            Movie backToTheFutureIII = new()
            {
                MovieId = 3,
                Title = "Back to the Future III",
                Genre = "Science Fiction",
                ReleaseYear = 1990,
                Director = "Robert Zemeckis",
                Picture = "",
                Trailer = "",
                FranchiseId = 1
            };

            Movie theMatrix = new()
            {
                MovieId = 4,
                Title = "The Matrix",
                Genre = "Science Fiction",
                ReleaseYear = 1999,
                Director = "The Wachowskis",
                Picture = "",
                Trailer = "",
                FranchiseId = 2
            };
            Movie theMatrixReloaded = new()
            {
                MovieId = 5,
                Title = "The Matrix Reloaded",
                Genre = "Science Fiction",
                ReleaseYear = 2003,
                Director = "The Wachowskis",
                Picture = "",
                Trailer = "",
                FranchiseId = 2
            };
            Movie theMatrixRevolutions = new()
            {
                MovieId = 6,
                Title = "The Matrix Revolutions",
                Genre = "Science Fiction",
                ReleaseYear = 2003,
                Director = "The Wachowskis",
                Picture = "",
                Trailer = "",
                FranchiseId = 2
            };
            Movie theMatrixResurrections = new()
            {
                MovieId = 7,
                Title = "The Matrix Resurrections",
                Genre = "Science Fiction",
                ReleaseYear = 2021,
                Director = "Lana Wachowski",
                Picture = "",
                Trailer = "",
                FranchiseId = 2
            };

            Character KeanuReeves = new()
            {
                CharacterId = 1,
                Fullname = "Keanu Reeves",
                Gender = "Male",                
            };
            Character CarrieAnneMoss = new()
            {
                CharacterId = 2,
                Fullname = "Carrie-Anne Moss",
                Gender = "Female",
            };

            Character MichaelAndrewFox = new()
            {
                CharacterId = 3,
                Fullname = "Michael Andrew Fox",
                Alias = "Michael J. Fox",
                Gender = "Male",
            };
            Character ChristopherLloyd = new()
            {
                CharacterId = 4,
                Fullname = "Christopher Lloyd",
                Gender = "Male",
            };

            modelBuilder.Entity<Movie>(entity =>
            {
                entity.ToTable("Movie");
            });

            modelBuilder.Entity<Franchise>()
                .HasData(backToTheFuture, matrix);

            modelBuilder.Entity<Movie>(entity =>
            {
                entity.ToTable("Movie");

                entity.HasIndex(e => e.MovieId, "Movie_CharacterId");

                entity.HasMany(m => m.Characters)
                    .WithMany(c => c.Movies)
                    .UsingEntity<Dictionary<string, object>>(
                        "MovieCharacter",
                        left => left.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                        right => right.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                        j =>
                        {
                            j.HasKey("MovieId", "CharacterId");
                            j.ToTable("MovieCharacter");
                            j.HasIndex(new[] { "CharacterId" }, "MovieCharacter_CharacterId");

                            j.HasData(
                                // Back to the future
                                new { CharacterId = 3, MovieId = 1 },
                                new { CharacterId = 3, MovieId = 2 },
                                new { CharacterId = 3, MovieId = 3 },
                                new { CharacterId = 4, MovieId = 1 },
                                new { CharacterId = 4, MovieId = 2 },
                                new { CharacterId = 4, MovieId = 3 },

                                // Matrix
                                new { CharacterId = 1, MovieId = 4 },
                                new { CharacterId = 1, MovieId = 5 },
                                new { CharacterId = 1, MovieId = 6 },
                                new { CharacterId = 1, MovieId = 7 },
                                new { CharacterId = 2, MovieId = 4 },
                                new { CharacterId = 2, MovieId = 5 },
                                new { CharacterId = 2, MovieId = 6 },
                                new { CharacterId = 2, MovieId = 7 }

                            );
                        }
                    );
            });
            
            
            modelBuilder.Entity<Movie>()
                .HasData(backToTheFutureI,
                 backToTheFutureII,
                 backToTheFutureIII,
                 theMatrix,
                 theMatrixReloaded,
                 theMatrixRevolutions,
                 theMatrixResurrections
            );
            
            modelBuilder.Entity<Character>()
                .HasData(KeanuReeves, CarrieAnneMoss, MichaelAndrewFox, ChristopherLloyd);
        }
    }
}
