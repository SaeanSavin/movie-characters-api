﻿using AutoMapper;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTO.Character;
using MovieCharactersAPI.Models.DTO.Movie;

namespace MovieCharactersAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterDTO>()
                .ForMember(
                dto => dto.Movies,
                opt => opt.MapFrom(m => m.Movies.Select(c => c.MovieId).ToList())
                );
            CreateMap<CharacterPutDTO, Character>();
            CreateMap<CharacterPostDTO, Character>();

            CreateMap<Character, CharacterSummaryDTO>();
        }
    }
}
