﻿using AutoMapper;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTO.Franchise;

namespace MovieCharactersAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseDTO>()
                .ForMember(
                dto => dto.Movies,
                opt => opt.MapFrom(m => m.Movies.Select(c => c.MovieId))
                );
            CreateMap<FranchisePutDTO, Franchise>();
            CreateMap<FranchisePostDTO, Franchise>();

            CreateMap<Franchise, FranchiseSummaryDTO>();
        }
    }
}
