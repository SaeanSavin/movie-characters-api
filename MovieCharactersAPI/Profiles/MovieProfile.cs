﻿using AutoMapper;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Models.DTO.Movie;

namespace MovieCharactersAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieDTO>()
                .ForMember(
                dto => dto.Characters,
                opt => opt.MapFrom(m => m.Characters.Select(c => c.CharacterId))
                )
                .ForMember(
                dto => dto.FranchiseId,
                opt => opt.MapFrom(m => m.Franchise.FranchiseId)
                );
            CreateMap<MoviePutDTO, Movie>();
            CreateMap<MoviePostDTO, Movie>();

            CreateMap<Movie, MovieSummaryDTO>();
        }
    }
}
