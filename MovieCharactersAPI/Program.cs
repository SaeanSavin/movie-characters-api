using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Services.Characters;
using MovieCharactersAPI.Services.Franchises;
using MovieCharactersAPI.Services.Movies;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Configuring generated XML docs for Swagger

var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "Movie API",
        Description = "Simple API to manage movies",
        Contact = new OpenApiContact
        {
            Name = "Andreas S�ther",
            Url = new Uri("https://gitlab.com/SaeanSavin")
        },
        License = new OpenApiLicense
        {
            Name = "MIT 2022",
            Url = new Uri("https://opensource.org/licenses/MIT")
        }
    });
    options.IncludeXmlComments(xmlPath);
});

// Adding services to the container
builder.Services.AddDbContext<MovieDbContext>(
    opt => opt.UseSqlServer(
            builder.Configuration.GetConnectionString("MovieCharactersEf")
        )
    );

builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

builder.Host.ConfigureLogging(logging =>
{
    logging.ClearProviders();
    logging.AddConsole();
});

builder.Services.AddTransient<IMovieService, MovieServiceImpl>();
builder.Services.AddTransient<ICharacterService, CharacterServiceImpl>();
builder.Services.AddTransient<IFranchiseService, FranchiseServiceImpl>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
