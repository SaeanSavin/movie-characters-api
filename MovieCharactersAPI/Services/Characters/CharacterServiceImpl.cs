﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Services.Movies;
using MovieCharactersAPI.Util.Exceptions;

namespace MovieCharactersAPI.Services.Characters
{
    /// <summary>
    /// This class contains the business logic of a movie character.
    /// </summary>
	public class CharacterServiceImpl : ICharacterService
	{
        private readonly MovieDbContext _movieDbContext;
        
        private readonly ILogger<CharacterServiceImpl> _logger;

        public CharacterServiceImpl(MovieDbContext movieDbContext, ILogger<CharacterServiceImpl> logger)
		{
            _movieDbContext = movieDbContext;
            _logger = logger;
        }

        public async Task AddAsync(Character entity)
        {
            await _movieDbContext.AddAsync(entity);
            await _movieDbContext.SaveChangesAsync();
        }

        public async Task<ICollection<Character>> GetAllAsync()
        {
            return await _movieDbContext.Characters
                .Include(m => m.Movies)
                .ToListAsync();
        }

        public async Task<Character> GetByIdAsync(int id)
        {
            if (!await CharacterExitsAsync(id))
            {
                _logger.LogError("Character not found with Id: " + id);
                throw new CharacterNotFoundException("Character not found with Id: " + id);
            }
            return await _movieDbContext.Characters
                .Where(c => c.CharacterId == id)
                .Include(m => m.Movies)
                .FirstAsync();
        }

        public async Task<ICollection<Movie>> GetMoviesForCharacterAsync(int characterId)
        {
            if (!await CharacterExitsAsync(characterId))
            {
                _logger.LogError("Character not found with Id: " + characterId);
                throw new MovieNotFoundException("Character not found with Id: " + characterId);
            }
            Character character = await _movieDbContext.Characters
                .Where(c => c.CharacterId == characterId)
                .Include(c => c.Movies)
                .FirstAsync();

            return character.Movies.ToList();

        }

        public async Task UpdateAsync(Character character)
        {
            if (!await CharacterExitsAsync(character.CharacterId))
            {
                _logger.LogError("Character not found with Id: " + character.CharacterId);
                throw new MovieNotFoundException("Character not found with Id: " + character.CharacterId);
            }

            _movieDbContext.Entry(character).State = EntityState.Modified;
            await _movieDbContext.SaveChangesAsync();
        }

        public async Task UpdateMoviesForCharacterAsync(int[] moviesIds, int characterId)
        {
            if (!await CharacterExitsAsync(characterId))
            {
                _logger.LogError("Character not found with id: " + characterId);
                throw new CharacterNotFoundException("Character not found with id: " + characterId);
            }

            // Retrive the character by id
            Character character = await _movieDbContext.Characters
                .Where(c => c.CharacterId == characterId)
                .Include(c => c.Movies)
                .FirstAsync();

            List<Movie> movies = moviesIds
                .ToList()
                .Select( mid => _movieDbContext.Movies
                .Where(m => m.MovieId == mid).First())
                .ToList();

            // Set the franchises movies
            character.Movies = movies;
            _movieDbContext.Entry(character).State = EntityState.Modified;

            // Save all the changes
            await _movieDbContext.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int characterId)
        {
            var character = await _movieDbContext.Characters.FindAsync(characterId);

            if (character == null)
            {
                _logger.LogError("Character not found with Id: " + characterId);
                throw new CharacterNotFoundException("Character not found with Id: " + characterId);
            }
            _movieDbContext.Remove(character);
            await _movieDbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Simple utility method to help check if a character exists with provided Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private async Task<bool> CharacterExitsAsync(int id)
        {
            return await _movieDbContext.Characters.AnyAsync(c => c.CharacterId == id);
        }
    }
}

