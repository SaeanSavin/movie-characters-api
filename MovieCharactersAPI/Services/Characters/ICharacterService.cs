﻿using System;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Util.Exceptions;

namespace MovieCharactersAPI.Services.Characters
{
    public interface ICharacterService : ICrudService<Character, int>
    {
        Task<ICollection<Movie>> GetMoviesForCharacterAsync(int characterId);

        Task UpdateMoviesForCharacterAsync(int[] characterIds, int characterId);
    }
}

