﻿using System;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Services.Movies;
using MovieCharactersAPI.Util.Exceptions;

namespace MovieCharactersAPI.Services.Franchises
{
    /// <summary>
    /// This contains the business logic for a movie franchise.
    /// </summary>
    public class FranchiseServiceImpl : IFranchiseService
    {
        private readonly MovieDbContext _movieDbContext;
        
        private readonly ILogger<FranchiseServiceImpl> _logger;

        public FranchiseServiceImpl(MovieDbContext movieDbContext, ILogger<FranchiseServiceImpl> logger)
        {
            _movieDbContext = movieDbContext;
            _logger = logger;
        }

        public async Task AddAsync(Franchise franchise)
        {
            await _movieDbContext.AddAsync(franchise);
            await _movieDbContext.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            Franchise? franchise = await _movieDbContext.Franchises
                .Where(f => f.FranchiseId == id)
                .Include(m => m.Movies)
                .FirstAsync();

            if (franchise == null)
            {
                _logger.LogError("Franchise not found with Id: " + id);
                throw new MovieNotFoundException("Franchise not found with Id: " + id);
            }

            
            // Set all foreign keys to null before delete 
            franchise.Movies = franchise.Movies.Where(m => m.FranchiseId != null && m.FranchiseId == id)
                .Select(m => { m.FranchiseId = null; return m; })
                .ToList();
            
            _movieDbContext.Franchises.Remove(franchise);
            await _movieDbContext.SaveChangesAsync();
        }

        public async Task<ICollection<Franchise>> GetAllAsync()
        {
            return await _movieDbContext.Franchises
                .Include(f => f.Movies)
                .ToListAsync();
        }

        public async Task<Franchise> GetByIdAsync(int id)
        {
            if (!await FranchiseExistsAsync(id))
            {
                _logger.LogError("Franchise not found with Id: " + id);
                throw new FranchiseNotFoundException("Franchise not found with Id: " + id);
            }
            return await _movieDbContext.Franchises
                .Where(f => f.FranchiseId == id)
                .Include(m => m.Movies)
                .FirstAsync();
        }

        public async Task<ICollection<Movie>> GetMoviesForFranchiseAsync(int franchiseId)
        {
            if (!await FranchiseExistsAsync(franchiseId))
            {
                _logger.LogError("Franchise not found with Id: " + franchiseId);
                throw new FranchiseNotFoundException("Franchise not found with Id: " + franchiseId);
            }
            return await _movieDbContext.Movies
                .Where(m => m.FranchiseId == franchiseId)
                .Include(m => m.Characters)
                .ToListAsync();
        }
        
        public async Task<ICollection<Character>> GetCharactersForFranchiseAsync(int franchiseId)
        {
            if (!await FranchiseExistsAsync(franchiseId))
            {
                _logger.LogError("Franchise not found with Id: " + franchiseId);
                throw new FranchiseNotFoundException("Franchise not found with Id: " + franchiseId);
            }

            return await _movieDbContext.Movies
                .Where(movie => movie.FranchiseId == franchiseId)
                .Include(movie => movie.Characters)
                .SelectMany(movie => movie.Characters)
                .Distinct()
                .ToListAsync();
        }

        public async Task UpdateAsync(Franchise franchise)
        {
            if (!await FranchiseExistsAsync(franchise.FranchiseId))
            {
                _logger.LogError("Franchise not found with id: " + franchise.FranchiseId);
                throw new FranchiseNotFoundException("Franchise not found with id: " + franchise.FranchiseId);
            }
            _movieDbContext.Entry(franchise).State = EntityState.Modified;
            await _movieDbContext.SaveChangesAsync();
        }

        public async Task UpdateMoviesInFranchiseAsync(int[] movieIds, int franchiseId)
        {
            if (!await FranchiseExistsAsync(franchiseId))
            {
                _logger.LogError("Franchise not found with id: " + franchiseId);
                throw new FranchiseNotFoundException("Franchise not found with id: " + franchiseId);
            }

            // Retrive the franchise by id
            Franchise franchise = await _movieDbContext.Franchises
                .Where(f => f.FranchiseId == franchiseId)
                .Include(f => f.Movies)
                .FirstAsync();

            List<Movie> movies = movieIds
                .ToList()
                .Select(mid => _movieDbContext.Movies
                .Where(m => m.MovieId == mid).First())
                .ToList();

            // Set the franchises movies
            franchise.Movies = movies;
            _movieDbContext.Entry(franchise).State = EntityState.Modified;

            // Save all the changes
            await _movieDbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Simple utility method to help check if a franchise exists with provided Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private async Task<bool> FranchiseExistsAsync(int id)
        {
            return await _movieDbContext.Franchises.AnyAsync(f => f.FranchiseId == id);
        }
    }
}

