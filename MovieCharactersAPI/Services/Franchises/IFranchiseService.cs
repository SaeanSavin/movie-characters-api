﻿using System;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Util.Exceptions;

namespace MovieCharactersAPI.Services.Franchises
{
    public interface IFranchiseService : ICrudService<Franchise, int>
    {
        /// <summary>
        /// Desc.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns>A collection of students</returns>
        /// <exception cref="MovieNotFoundException">Thrown when a professor does not exist with the specified Id in the database.</exception>
        Task<ICollection<Movie>> GetMoviesForFranchiseAsync(int franchiseId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        Task<ICollection<Character>> GetCharactersForFranchiseAsync(int franchiseId);

        /// <summary>
        /// Updates the ... This is a complete replacement, so a full list is required.
        /// </summary>
        /// <param name="movieIds"></param>
        /// <param name="franchiseId"></param>
        /// <exception cref="FranchiseNotFoundException">Thrown when a professor does not exist with the specified Id in the database.</exception>
        Task UpdateMoviesInFranchiseAsync(int[] movieIds, int franchiseId);


    } 
}

