﻿using System;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Util.Exceptions;

namespace MovieCharactersAPI.Services.Movies
{
    public interface IMovieService : ICrudService<Movie, int>
    {
        /// <summary>
        /// Desc.
        /// </summary>
        /// <param name="movieId"></param>
        /// <returns>A collection of students</returns>
        /// <exception cref="MovieNotFoundException">Thrown when a professor does not exist with the specified Id in the database.</exception>
        Task<ICollection<Character>> GetCharactersInMovieAsync(int movieId);

        /// <summary>
        /// Updates the ... This is a complete replacement, so a full list is required.
        /// </summary>
        /// <param name="characterIds"></param>
        /// <param name="movieId"></param>
        /// <exception cref="CharacterNotFoundException">Thrown when a professor does not exist with the specified Id in the database.</exception>
        Task UpdateCharactersAsync(int[] characterIds, int movieId);

        /// <summary>
        /// Updates the ... . This is a complete replacement, so a full list is required.
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="movieId"></param>
        /// <exception cref="FranchiseNotFoundException">Thrown when a professor does not exist with the specified Id in the database.</exception>
        Task UpdateFranchiseAsync(int franchiseId, int movieId);




    }
}

