﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Models;
using MovieCharactersAPI.Models.Domain;
using MovieCharactersAPI.Util.Exceptions;

namespace MovieCharactersAPI.Services.Movies
{
    /// <summary>
    /// This class contains the business logic for a movie.
    /// </summary>
    public class MovieServiceImpl : IMovieService
    {
        private readonly MovieDbContext _movieDbContext;
       
        private readonly ILogger<MovieServiceImpl> _logger;

        public MovieServiceImpl(MovieDbContext movieDbContext, ILogger<MovieServiceImpl> logger)
        {
            _movieDbContext = movieDbContext;
            _logger = logger;
        }

        public async Task AddAsync(Movie entity)
        {
            await _movieDbContext.AddAsync(entity);
            await _movieDbContext.SaveChangesAsync();
        }

        public async Task<ICollection<Movie>> GetAllAsync()
        {
            return await _movieDbContext.Movies
                .Include(m => m.Franchise)
                .Include(c => c.Characters)
                .ToListAsync();
        }

        public async Task<Movie> GetByIdAsync(int id)
        {
            if (!await MovieExistsAsync(id))
            {
                _logger.LogError("Movie not found with Id: " + id);
                throw new MovieNotFoundException("Movie not found with Id: " + id);
            }

            return await _movieDbContext.Movies
                .Where(m => m.MovieId == id)
                .Include(f => f.Franchise)
                .Include(c => c.Characters)
                .FirstAsync();
        }

        public async Task<ICollection<Character>> GetCharactersInMovieAsync(int movieId)
        {
            if (!await MovieExistsAsync(movieId))
            {
                _logger.LogError($"Movie not found with Id: {movieId}");
                throw new MovieNotFoundException("Movie not found with Id: " + movieId);
            }

            Movie movie = await _movieDbContext.Movies.Where
                (m => m.MovieId == movieId).Include(c => c.Characters).FirstAsync();

            return movie.Characters.ToList();
        }

        public async Task UpdateAsync(Movie movie)
        {
            if (!await MovieExistsAsync(movie.MovieId))
            {
                _logger.LogError("Movie not found with Id: " + movie.MovieId);
                throw new MovieNotFoundException("Movie not found with Id: " + movie.MovieId);
            }
            
            _movieDbContext.Entry(movie).State = EntityState.Modified;
            await _movieDbContext.SaveChangesAsync();
        }

        public async Task UpdateCharactersAsync(int[] characterIds, int movieId)
        {
            if (!await MovieExistsAsync(movieId))
            {
                _logger.LogError("Movie not found with Id: " + movieId);
                throw new MovieNotFoundException("Movie not found with Id: " + movieId);
            }

            Movie movie = await _movieDbContext.Movies
                .Where(m => m.MovieId == movieId)
                .Include(m => m.Characters)
                .FirstAsync();

            List<Character> characters = characterIds
                .ToList()
                .Select(cid => _movieDbContext.Characters
                .Where(c => c.CharacterId == cid).First())
                .ToList(); 

            movie.Characters = characters;
            _movieDbContext.Entry(movie).State = EntityState.Modified;

            await _movieDbContext.SaveChangesAsync();
        }

        public async Task UpdateFranchiseAsync(int franchiseId, int movieId)
        {
            // Log and throw pattern
            if (!await MovieExistsAsync(movieId))
            {
                _logger.LogError("Movie not found with Id: " + movieId);
                throw new MovieNotFoundException("Movie not found with Id: " + movieId);
            }
            // First convert the int[] to List<Subject>

            // Could utilize the SubjectNotFoundException here
            // if there is an Id in the array that doesnt have a subject.
            // That would require a different interaction with EF to check for exception.
            // It is not done in this implementation.
           
            // Get movie for Id
            Movie movie = await _movieDbContext.Movies
                .Where(m => m.MovieId == movieId)
                .FirstAsync();
            Franchise franchise = await _movieDbContext.Franchises
                .Where(f => f.FranchiseId == franchiseId)
                .FirstAsync();
            // Set the movies franchise
            movie.Franchise = franchise;
            _movieDbContext.Entry(movie).State = EntityState.Modified;
            // Save all the changes
            await _movieDbContext.SaveChangesAsync();
        }

        public async Task DeleteByIdAsync(int movieId)
        {
            var movie = await _movieDbContext.Movies.FindAsync(movieId);
            // Log and throw pattern
            if (movie == null)
            {
                _logger.LogError("Movie not found with Id: " + movieId);
                throw new MovieNotFoundException("Movie not found with Id: " + movieId);
            }
            // We set our entities to have nullable relationships
            // so it removes the FKs when delete it.
            _movieDbContext.Movies.Remove(movie);
            await _movieDbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Simple utility method to help check if a movie exists with provided Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>If professor exists</returns>
        private async Task<bool> MovieExistsAsync(int id)
        {
            return await _movieDbContext.Movies.AnyAsync(m => m.MovieId == id);
        }
    }
}
