
## Movie Character API Assignment
This projects is the third and final assignment of the backend section of the Accelerate Program at Noroff. 
The solution consists of three main concepts, Movie Characters, Movies and Franchises. Movie characters appear in movies which can be part of movie franchises. 

## Install
Open the project in Visual Studio 2022. Run the following command the IDEs packet manager.

```
dotnet restore
dotnet build
update-database
```

The connection string in appsettings.json must correspond to the local instance on your computer.
```
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft.AspNetCore": "Warning"
    }
  },
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "MovieCharactersEf": "Data Source = <Local name instance of database>; Initial Catalog = MovieCharactersEf; Integrated Security = True; Trust Server Certificate = True;"
  }
}
```

## Contributers
Andreas Sæther (@saeansavin) & Knut Formo Buene (@knutfb)

## License
MIT 2022
